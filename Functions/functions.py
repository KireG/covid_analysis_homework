import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from typing import Union


def format_number(number: int) -> int:
    """Format a number with spaces and dots."""
    num_str = str(number)
    formatted_number = ''
    while num_str:
        # Extract last three digits
        chunk = num_str[-3:]
        num_str = num_str[:-3]
        # Add dots if there are more digits remaining
        if num_str:
            formatted_number = '.' + chunk + formatted_number
        else:
            formatted_number = chunk + formatted_number
    return formatted_number


def print_sum(data: pd.DataFrame, *args: str) -> pd.DataFrame:
    totals = []
    columns = []
    for arg in args:
        su = data[arg].sum()
        totals.append(su)
        columns.append(arg)
    args_sums = pd.DataFrame([totals], columns=columns)
    return args_sums


def sum_by(key: str, data: pd.DataFrame, *args: str) -> pd.DataFrame:
    unique_key = data[key].unique()
    returned_list = {key: unique_key}
    for arg in args:
        returned_list[arg] = []
    for item in unique_key:
        filtered = data[data[key] == item]
        for arg in args:
            returned_list[arg].append(filtered[arg].sum())
    return pd.DataFrame(returned_list)

