import pandas as pd
from typing import Optional
from Models.interfaces.ConnectorInterface import ConnectorInterface


class Connect:
    def __init__(self, connector: ConnectorInterface) -> None:
        self.connector = connector

    def make_connection(self) -> Optional[pd.DataFrame]:
        return self.connector.connect()

