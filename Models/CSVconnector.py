import pandas as pd
from Models.interfaces.ConnectorInterface import ConnectorInterface


class CSVconnector(ConnectorInterface):
    def __init__(self, file: str) -> None:
        self.csv_file = file

    def connect(self) -> pd.DataFrame:
        return pd.read_csv(self.csv_file)

