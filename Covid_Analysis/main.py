import sys
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from Models.Connect import Connect
from Models.CSVconnector import CSVconnector
from Functions.functions import print_sum, sum_by

if __name__ == "__main__":
    process = True
    option = ''
    con = Connect(CSVconnector('../Data/covid_data.csv'))
    data = con.make_connection()
    while process:
        option = input("Insert 'a' for Summary, 'b' for Daily Cases, 'c' for Top 10 Countries, 'd' for data by Country "
                       "od 'q' to quit").lower()

        if option == 'q':
            process = False

        if option == 'a':
            # 2 Summary Statistics View
            all_sums = print_sum(data, 'confirmed_cases', 'deaths', 'recovered')
            print(all_sums)
            all_sums.plot.bar()
            plt.title('Summary')
            plt.xlabel('Explanations')
            plt.ylabel('Numbers in millions')
            plt.show()

        if option == 'b':
            # 3 Daily Cases Plotting
            # dt = data.groupby(["date"]).sum()
            unique_dates_df = sum_by('date', data, 'confirmed_cases', 'deaths', 'recovered')
            print(unique_dates_df)
            unique_dates_df.plot()
            plt.show()

        if option == 'c':
            # 4 Top 10 Countries
            unique_countries_df = sum_by('country', data, 'confirmed_cases')
            print(unique_countries_df)
            top_10 = unique_countries_df.sort_values(by='confirmed_cases', ascending=False)[:10]
            top_10.plot(x='country', y='confirmed_cases', kind='bar', figsize=(10, 10))
            plt.title('Countries with the highest confirmed cases')
            plt.xlabel('Country')
            plt.ylabel('Numbers of confirmed cases in millions')
            plt.show()

        while option == 'd':
            # 5 Country-wise Search
            cnt = input("Insert name of the country or 'q' to quit").capitalize()
            if cnt.lower() == 'q':
                option = ''
            is_contained = cnt in data['country'].values
            if is_contained:
                by_country = data[data['country'] == cnt]
                date = np.array(by_country['date'])
                confirmed = np.array(by_country['confirmed_cases'])
                deaths = np.array(by_country['deaths'])
                recovered = np.array(by_country['recovered'])
                by_country_df = pd.DataFrame({'date': date, 'confirmed_cases': confirmed, 'deaths': deaths,
                                              'recovered': recovered})
                print(by_country_df)
                fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(nrows=2, ncols=2, figsize=(15, 15))
                ax1.plot(date, confirmed)
                ax1.set_title('Confirmed')
                ax2.plot(date, deaths)
                ax2.set_title('Deaths')
                ax3.plot(date, recovered)
                ax3.set_title('Recovered')
                ax4.axis('off')
                plt.tight_layout()
                plt.style.use('seaborn')
                plt.show()
            else:
                print("Please insert valid data ow q to quit")
